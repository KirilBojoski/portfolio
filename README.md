# Portfolio

### Hi 👋, I'm Kiril Bojoski

<p>I am currently studying at CodersBay Vienna as an app developer, and my program will run until December 2023. Throughout my studies, I am gaining knowledge and skills in frontend and backend development, databases, and agile project management with Scrum. I am a highly motivated and creative person who thrives on teamwork, and I am excited to use these abilities to contribute to the development of innovative applications.</p>

# Projects

## Portfolio Website Project
* [Portfolio Website](http://kirilbojoski.at/)
<p>Recently, I decided to work on improving my skills in HTML, CSS, Bootstrap, and JavaScript. To do so, I thought it would be a great idea to create a website from scratch that would serve as an exercise to hone my skills.
The result of my efforts is a cool and simple portfolio website that I'm really proud of.</p>
<p>But I'm not done yet! I plan to continue working on this project, adding new features and experimenting with different design elements. I want to keep challenging myself and pushing the limits of what I can do with these programming languages. Overall, I'm thrilled with how this project has turned out so far. It's been an excellent learning experience.</p>

### Java Project
* [Pac-Man](https://gitlab.com/KirilBojoski/pacman)
<p>Our second semester group project in Pac-Man was focused on implementing a search algorithm (Dijkstra Algorithm) , and object-oriented programming in Java. Through this project, we were able to deepen our understanding of both search algorithms and OOP concepts, while also having fun with the classic Pac-Man game. It was a challenging and rewarding experience, and we're proud of the results we achieved.</p>

### Kotlin Jetpack Compose Project
* [MusicPlayer](https://gitlab.com/KirilBojoski/musicplayer)
<p>During my second semester project in the Kotlin Jetpack Compose framework, I developed a music player that helped me learn about Android app development. I gained knowledge in reading external storage and worked with .csv files as my database, which was a fun and exciting experience. This project also allowed me to enhance my understanding of Object-Oriented Programming (OOP) concepts, as well as the Model-View-ViewModel (MVVM) architecture and states. Overall, it was a great learning opportunity, and I enjoyed the process of building the music player using Kotlin Jetpack Compose.</p>

### Fullstack Spring Boot and Vue.js Project
* [AdministrationPlatform](https://gitlab.com/Daniel.A.92/codersbayadministration)

<p>During my first semester, I had the opportunity to work on a group project where we developed a web application using Java and Springboot framework as the backend and Vue.js as the frontend framework. This project was a great learning experience for me as it helped me to understand the basic concepts of CRUD(Create, Read, Update, Delete)operations and how frontend and backend requests work together to deliver the desired functionality.

Additionally, I gained valuable knowledge about database management and database relations. Overall, this project helped me to build a strong foundation in web development and enhanced my skills in both frontend and backend technologies.</p>



# Languages and Tools

### Web 
<p align="left">
<a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> 
<a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> 
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> 
<a href="https://vuejs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg" alt="vuejs" width="40" height="40"/> </a> 
<a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a> 
<a href="https://www.php.net" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-original.svg" alt="php" width="40" height="40"/> </a> 
<a href="https://laravel.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/laravel/laravel-plain-wordmark.svg" alt="laravel" width="40" height="40"/> </a> 
</p>


### Java
<p align="left">
<a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> 
<a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="40" height="40"/> </a> 
<a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> 
</p>

### Kotlin
<p align="left">
<a href="https://kotlinlang.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-icon.svg" alt="kotlin" width="40" height="40"/> </a> 
<a href="https://developer.android.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original-wordmark.svg" alt="android" width="40" height="40"/> </a> 
</p>

### Database
<p align="left">
<a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> 
<a href="https://www.postgresql.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="40" height="40"/> </a>
</p>

### Version Control
<p align="left">
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a>
</p>

### Design and Prototyping
<p align="left">
<a href="https://www.figma.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="40" height="40"/> </a> 
<a href="https://www.adobe.com/in/products/illustrator.html" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/adobe_illustrator/adobe_illustrator-icon.svg" alt="illustrator" width="40" height="40"/> </a> 
<a href="https://www.photoshop.com/en" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="40" height="40"/> </a>
</p>
